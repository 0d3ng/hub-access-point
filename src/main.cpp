#include <Arduino.h>
#include <Wire.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <SimpleDHT.h>
#include <Adafruit_GFX.h>     // library OLED
#include <Adafruit_SSD1306.h> // library font OLED

const char *ssid = "ap-polinema";
const char *password = "polinema";
String ip;

#define pinDHT 10 // pin SDD3

// Konstruktor instance Sensor DHT11
SimpleDHT11 dht11(pinDHT);

byte t = 0;
byte h = 0;

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

unsigned long previousMillis = 0;

// OLED 0.96"
Adafruit_SSD1306 display(128, 64, &Wire, -1);

// Updates DHT readings every 10 seconds
const long interval = 1500;

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
    html {
     font-family: Arial;
     display: inline-block;
     margin: 0px auto;
     text-align: center;
    }
    h2 { font-size: 3.0rem; }
    p { font-size: 3.0rem; }
    .units { font-size: 1.2rem; }
    .dht-labels{
      font-size: 1.5rem;
      vertical-align:middle;
      padding-bottom: 15px;
    }
  </style>
</head>
<body>
  <h2>Polinema DHT Server</h2>
  <p>
    <span class="dht-labels">Temperature</span> 
    <span id="temperature">%TEMPERATURE%</span>
    <sup class="units">&deg;C</sup>
  </p>
  <p>
    <span class="dht-labels">Humidity</span>
    <span id="humidity">%HUMIDITY%</span>
    <sup class="units">%</sup>
  </p>
</body>
<script>
setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("temperature").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/temperature", true);
  xhttp.send();
}, 10000 ) ;

setInterval(function ( ) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("humidity").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "/humidity", true);
  xhttp.send();
}, 10000 ) ;
</script>
</html>)rawliteral";

// Replaces placeholder with DHT values
String processor(const String &var)
{
  //Serial.println(var);
  if (var == "TEMPERATURE")
  {
    return String(t);
  }
  else if (var == "HUMIDITY")
  {
    return String(h);
  }
  return String();
}

void updateOLED();

void setup()
{
  // Serial port for debugging purposes
  Serial.begin(115200);
  Wire.begin();

  /*-------------------------
    Inisialisasi layar OLED
    -------------------------*/
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.clearDisplay();

  WiFi.softAP(ssid, password);

  IPAddress IP = WiFi.softAPIP();
  ip = IP.toString();
  Serial.print("AP IP address: ");
  Serial.println(IP);

  // Print ESP8266 Local IP Address
  Serial.println(WiFi.localIP());

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->send_P(200, "text/html", index_html, processor); });
  server.on("/temperature", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->send_P(200, "text/plain", String(t).c_str()); });
  server.on("/humidity", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->send_P(200, "text/plain", String(h).c_str()); });

  // Start server
  server.begin();
}

void loop()
{
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval)
  {
    // save the last time you updated the DHT values
    previousMillis = currentMillis;
    byte suhu = 0;
    byte hum = 0;

    int err = SimpleDHTErrSuccess;
    if ((err = dht11.read(&suhu, &hum, NULL)) != SimpleDHTErrSuccess)
    {
      Serial.print("Read DHT11 failed, err=");
      Serial.println(err);
      delay(1000);
      return;
    }

    // Memastikan suhu dan kelembaban valid
    // bila bernilai 0 maka diambil dari nilai sebelumnya
    if (suhu != 0 || hum != 0)
    {
      t = suhu;
      h = hum;
    }

    Serial.print("Sample OK: ");
    Serial.print((int)t);
    Serial.print(" *C, ");
    Serial.print((int)h);
    Serial.println(" H");

    updateOLED();
  }
}

void updateOLED()
{
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);

  display.setCursor(0, 5);
  display.print("AP");
  display.setCursor(50, 5);
  display.print(ssid);
  display.setCursor(0, 15);
  display.print("Pass");
  display.setCursor(50, 15);
  display.print(password);
  display.setCursor(0, 25);
  display.print("IP");
  display.setCursor(50, 25);
  display.print(ip);
  display.setCursor(0, 45);
  display.print("Temperature");
  display.setCursor(75, 45);
  display.print(String(t));
  display.drawCircle(100, 44, 2, SSD1306_WHITE);
  display.setCursor(105, 45);
  display.print("C");

  display.setCursor(0, 55);
  display.print("Humidity");
  display.setCursor(75, 55);
  display.print(String(h));
  display.setCursor(100, 55);
  display.print("H");

  display.display();
}